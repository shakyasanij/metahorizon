/**
 * Created by Asus on 6/7/2018.
 */
$(document).ready(function () {
    $('#usertab').DataTable({
        "processing": true,
        "ajax": {
            "url": "/users",
            "type": "GET",
            "dataSrc": ""
        },
        "columns": [{
            "data": "name"
        }, {
            "data": "userName"
        }, {
            "data": "email"
        }, {
            "data": "phoneNumber"
        }, {
            "data": "address"
        }],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'pdfHtml5',
                title: function () {
                    return "laliguras";
                },
                orientation: 'Portrait',
                pageSize: 'A1',
                titleAttr: 'PDF',
                label:"PDF"
            },
            {
                extend: 'print',
                titleAttr: 'PRINT'
            }
        ]
    });
})


